﻿namespace DungeonCrawler
{
    using System;

    class Program
    {
        [STAThread]
        static void Main()
        {
            using (var game = new Game())
            {
                game.Run();
            }
        }
    }
}
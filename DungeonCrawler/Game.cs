﻿namespace DungeonCrawler
{
    using Nez;

    public class Game : Core
    {

        public Game() : base(windowTitle: "Dungeon Crawler")
        {
            var policy = Scene.SceneResolutionPolicy.ShowAllPixelPerfect;
            Scene.setDefaultDesignResolution(800, 600, policy);
        }

        protected override void Initialize()
        {
            Window.AllowUserResizing = true;
            IsMouseVisible = true;
            base.Initialize();
        }
    }
}